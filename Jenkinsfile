pipeline {
  agent any
  environment {
    HEROKU_APP = 'bsa-docker-example'
    HEROKU_USER = credentials('HEROKU_USER')
    HEROKU_KEY = credentials('HEROKU_KEY')
  }

  stages {
    stage('Prepare environment') {
      steps {
        sh 'node -v || apk add --update nodejs'
        sh 'npm -v || apk add --update npm'
      }
    }

    stage('Install dependencies') {
      steps {
        sh 'npm install'
      }
    }

    stage('Run test') {
      steps {
        sh 'npm run test'
      }
    }

    stage('Build sources') {
      steps {
        sh 'npm run build'
      }
    }

    stage('Build & push docker image') {
      when {
        branch 'main';
      }

      steps {
        sh 'docker login --username=$HEROKU_USER --password=$HEROKU_KEY registry.heroku.com'
        sh 'docker build -t registry.heroku.com/$HEROKU_APP/web .'
        sh 'docker push registry.heroku.com/$HEROKU_APP/web'
      }
    }

    stage('Deploy to Heroku') {
      when {
        branch 'main';
      }

      steps {
        sh '''
          HEROKU_IMAGE_ID=$(docker inspect registry.heroku.com/$HEROKU_APP/web --format={{.Id}})
          curl -f -X PATCH https://api.heroku.com/apps/$HEROKU_APP/formation \
            -u "$HEROKU_USER:$HEROKU_KEY" \
            -d '{"updates": [{"type":"web","docker_image":"'$HEROKU_IMAGE_ID'"}]}' \
            -H "Content-Type: application/json" \
            -H "Accept: application/vnd.heroku+json; version=3.docker-releases"
        '''
      }
    }

  }
}