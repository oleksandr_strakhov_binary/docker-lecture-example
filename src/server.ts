import express from "express";

const app = express();

app.get('/helloWorld', (req, res) => {
    res.send('Hello World!');
});

export { app };
