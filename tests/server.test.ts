import { app } from '../src/server';
import supertest from 'supertest';

const request = supertest(app);

describe('HTTP Server', () => {
    describe('GET /helloWorld', () => {
        it('returns status code 200', async () => {
            const res = await request.get('/helloWorld');
            expect(res.statusCode).toBe(200);
        });

        it('returns "Hello World!"', async () => {
            const res = await request.get('/helloWorld');
            expect(res.text).toBe('Hello World!');
        });
    });
});
